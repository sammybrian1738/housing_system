const mongoose = require('mongoose')

const Schema = mongoose.Schema

const userSchema = new Schema({
    email : String,
    password : String
})

const landlordModel = mongoose.model('landlord', userSchema)
module.exports = landlordModel