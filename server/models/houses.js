const mongoose = require('mongoose')

const Schema = mongoose.Schema
const houseSchema = new Schema({
    houseName : String,
    firstName : String, 
    lastName : String,
    houseRent : String,
    location : String,
    houseType : String,
    phone : Number,
    vacancies : Number,
    imageUrl : String
})

const houseModel = mongoose.model('house', houseSchema)

module.exports = houseModel