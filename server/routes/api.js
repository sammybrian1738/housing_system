const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')
const Landlord = require('../models/user')
const House = require('../models/houses')

const router = express.Router();

mongoose.connect('mongodb://localhost/housing')
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.log('Could not connect to MongoDB...', err));

function verifyToken(req, res, next){
    if(!req.headers.authorization){
        res.status(401).send('Unauthorized request')
    }
    
    let token = req.headers.authorization.split(' ')[1]
    
    if(token ===' null'){
        res.status(401).send('Unauthorized request')
    }

    let payload = jwt.verify(token, 'secretKey')
    
    if(!payload){
        res.status(401).send('Unauthorized request')
    }

    req.userId = payload.subject
    next()
}

router.get('/', (req, res) => {
    res.send('Hello from API route');
});

router.get('/houses', (req, res) => {
    async function getHouses(){
        const houses = await House
            .find()
            .sort({houseName : 1})

        res.json(houses)
    }
    getHouses()
})

router.post('/register', (req, res) => {
    async function createUser(){
        let landlordData = req.body;
        let landlord = new Landlord(landlordData);
        await landlord.save((error, registeredUser) => {
            if(error){
                console.log(error)
            }
            else{
                let payload = {subject : registeredUser._id}
                let token = jwt.sign(payload, 'secretKey')
                res.status(200).send({token})
            }
        });
    }
    createUser();
});

router.post('/login', (req, res) => {
    let landlordData = req.body
    Landlord.findOne({ email : landlordData.email }, (error, user) => {
        if(error){
            console.log(error)
        }
        else{
            if(!user){
                res.status(401).send('Invalid email')
            }
            else{
                if(user.password !== landlordData.password){
                    res.status(401).send('Invalid password')
                }
                else{
                    let payload = {subject : user._id}
                    let token = jwt.sign(payload, 'secretKey')
                    res.status(200).send({token})
                }
            }
        }
    })
})

router.post('/houses', verifyToken, (req, res) => {
    async function createHouses(){
        let houseData = req.body
        let house = new House(houseData)
        await house.save((error, registeredHouse) => {
            if(error){
                console.log(error)
            }
            else{
                res.status(200).send(registeredHouse)
            }
        })
    }
    createHouses()
})

module.exports = router