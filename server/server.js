const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express();
app.use(express.json());
app.use(cors());

const api = require('./routes/api');

app.use('/api', api)

const port = 5000 

app.get('/', (req, res) => {
    res.send('Hello from server')
})

app.listen(port, () => console.log(`Server running on localhost:${port}`))