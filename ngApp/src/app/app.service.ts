import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IHouse } from './houses/house-interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  houseDetail : IHouse

  private _registerUrl = 'http://localhost:5000/api/register';
  private _loginUrl = 'http://localhost:5000/api/login';
  private _housesUrl = 'http://localhost:5000/api/houses'

  constructor(private _http : HttpClient,
              private _router : Router){ }

  registerLandlord(landlord){
    return this._http.post<any>(this._registerUrl, landlord)
  }

  loginLandlord(landlord){
    return this._http.post<any>(this._loginUrl, landlord)
  }

  onSubmitHouse(house){
    return this._http.post<any>(this._housesUrl, house)
  }

  getHouses(){
    return this._http.get<any>(this._housesUrl)
  }

  loggedIn(){
    return !!localStorage.getItem('token')
  }

  getToken(){
    return localStorage.getItem('token')
  }

  logoutUser(){
    localStorage.removeItem('token')
    this._router.navigate(['/apartments'])
  }

}
