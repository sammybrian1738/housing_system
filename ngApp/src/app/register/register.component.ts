import { Component, OnInit } from '@angular/core';
import { Landlord } from '../landlord';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerLandlordData = new Landlord('','')

  constructor(private _appService : AppService,
              private _router : Router) { }

  ngOnInit(): void {
  }

  registerLandlord(){
    this._appService.registerLandlord(this.registerLandlordData)
      .subscribe(
        res => {
          console.log(res)
          localStorage.setItem('token',res.token)
          this._router.navigate(['/registerHouse'])
        },
        err => console.log(err)
      )
  }

}
