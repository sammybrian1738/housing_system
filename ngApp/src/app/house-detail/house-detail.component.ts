import { Component, OnInit, Input } from '@angular/core';
import { IHouse } from '../houses/house-interface';

@Component({
  selector: 'house-detail',
  templateUrl: './house-detail.component.html',
  styleUrls: ['./house-detail.component.css'],
})
export class HouseDetailComponent implements OnInit {

  @Input() house : IHouse
  imageWidth : number = 200
  imageMargin : number = 2

  constructor() { }

  ngOnInit(): void {
  }

}
