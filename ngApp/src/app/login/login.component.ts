import { Component, OnInit } from '@angular/core';
import { Landlord } from '../landlord';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginLandlordData = new Landlord('','')
  errors : String

  constructor(private _appService : AppService,
              private _router : Router) { }

  ngOnInit(): void {
  }

  loginLandlord(){
    this._appService.loginLandlord(this.loginLandlordData)
      .subscribe(
        res => {
          console.log(res)
          localStorage.setItem('token', res.token)
          this._router.navigate(['/registerHouse'])
        },
        err => {
          this.errors = 'Invalid email or password'
          console.log(err)
        }  
      )
  }

}
