export interface IHouse{
    _id : String
    houseName : String,
    firstName : String, 
    lastName : String,
    houseRent : String,
    location : String,
    houseType : String,
    phone : Number,
    vacancies : Number,
    imageUrl : String
}