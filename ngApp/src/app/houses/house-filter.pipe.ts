import { Pipe, PipeTransform } from "@angular/core";
import { IHouse } from './house-interface';

@Pipe({
    name : 'houseTypeFilter'
})

export class HouseTypeFilterPipe implements PipeTransform{
    transform(value : IHouse[], filterBy : string) : IHouse[]{
        filterBy = filterBy? filterBy.toLocaleLowerCase() : null;

        return filterBy? value.filter((house : IHouse) => 
        house.houseType.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
    }
}