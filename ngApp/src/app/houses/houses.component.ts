import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { IHouse } from './house-interface';

@Component({
  selector: 'app-houses',
  templateUrl: './houses.component.html',
  styleUrls: ['./houses.component.css']
})
export class HousesComponent implements OnInit {

  houses : IHouse[]
  selectedHouse : IHouse
  types : []

  constructor(private _appService : AppService) { }

  ngOnInit(): void {
    this._appService.getHouses()
      .subscribe(
        res => this.houses = res,
        err => console.log(err)
      )
  }

  onSelectHouse(house : any){
    this.selectedHouse = house;
    console.log(this.selectedHouse);
  }

}
