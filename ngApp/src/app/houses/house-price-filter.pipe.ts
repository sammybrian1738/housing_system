import { Pipe, PipeTransform } from "@angular/core";
import { IHouse } from './house-interface';

@Pipe({
    name : 'housePriceFilter'
})

export class HousePriceFilterPipe implements PipeTransform{
    transform(value : IHouse[], filterBy : string) : IHouse[]{
        filterBy = filterBy? filterBy.toLocaleLowerCase() : null;

        return filterBy? value.filter((house : IHouse) => 
        house.houseRent.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;


        /*transform(value : IHouse[], filterBy : string) : IHouse[]{
            filterBy = filterBy? filterBy.toLocaleLowerCase() : null;

            return filterBy? value.filter((house : IHouse) => 
            house.price.toLocaleString(this.pricestring , Option? Intl.NumberFormat).indexOf(filterBy) !== -1) : value;
        }*/
    }
}