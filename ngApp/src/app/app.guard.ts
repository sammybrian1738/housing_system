import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { AppService } from './app.service'

@Injectable({
  providedIn: 'root'
})
export class AppGuard implements CanActivate {

  constructor(private _appService : AppService,
              private _router : Router){}

  canActivate(): boolean {
    if(this._appService.loggedIn()){
      return true
    }
    else{
      this._router.navigate(['/login'])
      return false
    }
  }
  
}
