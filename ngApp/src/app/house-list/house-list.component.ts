import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { IHouse } from '../houses/house-interface';

@Component({
  selector: 'house-list',
  templateUrl: './house-list.component.html',
  styleUrls: ['./house-list.component.css'],
  outputs : ['SelectHouse']
})
export class HouseListComponent implements OnInit {
  
  @Input() houses : IHouse[]
  @Output() SelectHouse : EventEmitter<IHouse> = new EventEmitter()

  imageWidth : number = 200
  imageMargin : number = 2
  houseType : string
  pricefilter : string

  constructor() { }

  ngOnInit(): void {
  }

  onSelect(house : IHouse){
    this.SelectHouse.emit(house)
  }

}
