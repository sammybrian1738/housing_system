import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule} from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AddHouseComponent } from './add-house/add-house.component';
import { AppService } from './app.service';
import { HousesComponent } from './houses/houses.component';
import { HousePriceFilterPipe } from './houses/house-price-filter.pipe';
import { HouseTypeFilterPipe } from './houses/house-filter.pipe';
import { HouseDetailComponent } from './house-detail/house-detail.component';
import { HouseListComponent } from './house-list/house-list.component';
import { AppGuard } from './app.guard';
import { TokenInterceptorService } from './token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    LoginComponent,
    RegisterComponent,
    AddHouseComponent,
    HousesComponent,
    HousePriceFilterPipe,
    HouseTypeFilterPipe,
    HouseDetailComponent,
    HouseListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AppService, AppGuard,
  {
    provide : HTTP_INTERCEPTORS,
    useClass : TokenInterceptorService,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
