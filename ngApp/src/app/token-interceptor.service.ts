import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { AppService } from './app.service';


@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private injector : Injector) { }

  intercept(req, next){
    let _appService = this.injector.get(AppService)
    let tokenizedRequest = req.clone({
      setHeaders : {
        Authorization :  `Bearer ${_appService.getToken()}` 
      }
    })
    return next.handle(tokenizedRequest)
  }
}
