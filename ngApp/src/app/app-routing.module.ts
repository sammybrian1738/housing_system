import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component'
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AddHouseComponent } from './add-house/add-house.component';
import { HousesComponent } from './houses/houses.component';
import { HouseDetailComponent } from './house-detail/house-detail.component';
import { AppGuard } from './app.guard';

const routes: Routes = [
  {
    path : '',
    redirectTo : 'welcome', 
    pathMatch : 'full'
  },
  {
    path : 'welcome',
    component : WelcomeComponent
  },
  {
    path : 'login',
    component : LoginComponent
  },
  {
    path : 'register',
    component : RegisterComponent
  },
  {
    path : 'registerHouse',
    component : AddHouseComponent,
    canActivate : [AppGuard]
  },
  {
    path : 'apartments',
    component : HousesComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
