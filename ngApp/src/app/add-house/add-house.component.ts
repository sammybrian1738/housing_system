import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { IHouse } from '../houses/house-interface';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-house',
  templateUrl: './add-house.component.html',
  styleUrls: ['./add-house.component.css']
})
export class AddHouseComponent implements OnInit {

  houseData : IHouse

  constructor(private _appService : AppService,
              private _router : Router) { }

  ngOnInit(): void {
  }

  onSubmit(){
    this._appService.onSubmitHouse(this.houseData)
      .subscribe(
        res => console.log(res),
        err => {
          if(err instanceof HttpErrorResponse){
            if(err.status === 401){
              this._router.navigate(['/login'])
            }
          }
        }
      )
  }

}
